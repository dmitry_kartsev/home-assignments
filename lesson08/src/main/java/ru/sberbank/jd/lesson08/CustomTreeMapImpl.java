package ru.sberbank.jd.lesson08;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CustomTreeMapImpl<K, V> implements CustomTreeMap<K, V> {

    /**
     * Создаем статический класс в котором будут хранится
     * сами элементы <K и V> и 3 ссылки на parent и
     * leftChild и rightChild (или правый и левый листок)
     */
    static class Node<K, V> {
        K key;
        V value;
        Node<K, V> leftChild;
        Node<K, V> rightChild;
        Node<K, V> parent;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * Создаем так же root, Comparator<k>
     * и поле size
     */
    private Node<K, V> root;
    private Comparator<K> comparator;
    private int size;

    /**
     * Создаем Конструктор
     */
    public CustomTreeMapImpl(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    /**
     * Переопределяем метод для возвращения размера
     * коллекции CustomTreeMapImpl
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Переопределяем метод для возвращения булевого
     * значения, пуста ли данная коллекция или нет
     * (true or false)
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Переопределяем метод для возвращения значения
     * по переданному в параметры ключу
     */
    @Override
    public V get(K key) {
        Node<K, V> getNode = getNodeByKey(key);
        return null == getNode ? null : getNode.value;
    }

    /**
     * Вынесем отдельно реализицию метода по поиску и возвращения
     * необходимого значения в Node
     */
    private Node<K, V> getNodeByKey(K key) {
        Node<K, V> actual = root;
        while (actual != null) {
            int compareResult = comparator.compare(key, actual.key);
            if (compareResult < 0)
                actual = actual.leftChild;
            else if (compareResult > 0)
                actual = actual.rightChild;
            else return actual;
        }
        return null;
    }

    /**
     * Переопределяем метод на добавление элементов
     * Node <K, V>
     */
    @Override
    public V put(K key, V value) {
        if (value == null)
            throw new NullPointerException("Key must not be null!");

        if (root == null) {
            root = new Node<>(key, value);
            size++;
            return null;
        }

        return insert(root, key, value);
    }

    /**
     * Вынесем отдельный метод insert для добавления элементов
     * по типу двоичного(бинарного) дерева
     */
    private V insert(Node<K, V> node, K key, V value) {
        int compareResult = comparator.compare(key, node.key);
        if (compareResult < 0) {
            if (null == node.leftChild) {
                node.leftChild = new Node<>(key, value);
                node.leftChild.parent = node;
                size++;
                return null;
            } else {
                return insert(node.leftChild, key, value);
            }
        } else if (compareResult > 0) {
            if (null == node.rightChild) {
                node.rightChild = new Node<>(key, value);
                node.rightChild.parent = node;
                size++;
                return null;
            } else {
                return insert(node.rightChild, key, value);
            }
        } else {
            V oldValue = node.value;
            node.value = value;
            return oldValue;
        }
    }

    /**
     * Переопределяем метод на удаление элемента
     * Node <K, V> по ключу
     */
    @Override
    public V remove(K key) {
        Node<K, V> x = getNodeByKey(key);
        if (x == null)
            return null;

        Node<K, V> y, parent = x.parent;

        //В случае, если (x.rightChild) не имеет правого потомка
        if (x.rightChild == null) {
            y = x.leftChild;
            if (parent == null) {
                root = y;
                if (root != null)
                    root.parent = null;
            } else {
                if (x == parent.leftChild)
                    parent.leftChild = y;
                else parent.rightChild = y;
                if (y != null)
                    y.parent = parent;
            }
        } else {

            //В случае, если (x) имеет правого потомока (y), а (y) не имеет левого потомка.
            y = x.rightChild;
            if (y.leftChild == null) {
                y.leftChild = x.leftChild;
                if (y.leftChild != null)
                    y.leftChild.parent = y;
                if (parent == null) {
                    root = y;
                    root.parent = null;
                } else {
                    if (parent.leftChild == x)
                        parent.leftChild = y;
                    else parent.rightChild = y;
                    if (y != null)
                        y.parent = parent;
                }
            } else {

                // В случае, если (x) имеет правого потомка (y),
                // а (y) имеет левого потомка.
                //
                // Метод 'findMinLeftChild' - на нахождение наименьшего
                // узла среди больших, который и будет преемником
                Node<K, V> z = findMinLeftChild(y);
                Node<K, V> zRight = z.rightChild;
                if (zRight != null) {
                    z.parent.leftChild = zRight;
                    zRight.parent = z.parent;
                } else {
                    z.parent.leftChild = null;
                }


                // В случае, если корень (root)
                // является удаляемым элементом
                //
                if (x.parent == null) {
                    root = z;
                } else {
                    if (x.parent.leftChild == x)
                        x.parent.leftChild = z;
                    else x.parent.rightChild = z;
                }
                y.parent = z;
                z.leftChild = x.leftChild;
                z.rightChild = x.rightChild;
                z.parent = x.parent;
            }
        }
        x.leftChild = null;
        x.rightChild = null;
        x.parent = null;
        size--;
        return x.value;
    }

    /**
     * Вынесем метод на нахождение наименьшего
     * узла среди больших, который будет преемником
     */
    Node<K, V> findMinLeftChild(Node<K, V> node) {
        Node<K, V> result = node;
        while (result.leftChild != null) {
            result = result.leftChild;
        }
        return result;
    }

    /**
     * Переопределяем метод для возвращения булевого
     * значения, содержится ли данный ключ в коллекции или нет
     * (true or false)
     */
    @Override
    public boolean containsKey(K key) {
        return get(key) != null;
    }

    /**
     * Переопределяем метод для возвращения булевого
     * значения, содержится ли данное значение в коллекции или нет
     * (true or false)
     */
    @Override
    public boolean containsValue(V value) {
        List<Object> values = Arrays.asList(values());
        return values.contains(value);
    }

    /**
     * Переопределяем метод для получения в виде
     * массива всех ключей
     */
    @Override
    public Object[] keys() {
        List keys = new ArrayList<K>(Math.max(size, 20));
        if (root != null)
            addKeysToList(root, keys);
        return keys.toArray();
    }

    /**
     * Вынесем отдельный метод для добавления всех
     * ключей в коллекцию в List
     */
    private void addKeysToList(Node<K, V> node, List<K> list) {
        if (node.leftChild != null)
            addKeysToList(node.leftChild, list);
        list.add(node.key);
        if (node.rightChild != null)
            addKeysToList
                    (node.rightChild, list);
    }

    /**
     * Переопределяем метод для получения в виде
     * массива всех значений
     */
    @Override
    public Object[] values() {
        List values = new ArrayList<V>(Math.max(size, 20));
        if (root != null)
            addValuesToList(root, values);
        return values.toArray();
    }

    /**
     * Вынесем отдельный метод для добавления всех
     * значений в коллекцию в List
     */
    private void addValuesToList(Node<K, V> node, List<V> list) {
        if (node.leftChild != null)
            addValuesToList(node.leftChild, list);
        list.add(node.value);
        if (node.rightChild != null)
            addValuesToList(node.rightChild, list);
    }
}