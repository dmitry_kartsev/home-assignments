package ru.sberbank.jd.lesson08;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class CustomTreeMapImplTest {
    private CustomTreeMap<Integer, String> customTreeMap =
            new CustomTreeMapImpl<>(Integer::compareTo);

    /**
     * Реализация теста по методу size.
     * Возвращает значение количества элементов
     * в коллекции
     */
    @Test
    public void size() {
        Assert.assertEquals(0, customTreeMap.size());

        customTreeMap.put(10, "Value_10");
        customTreeMap.put(-5, "Value_-5");
        customTreeMap.put(8, "Value_8");
        customTreeMap.put(7, "Value_7");
        customTreeMap.put(9, "Value_9");
        customTreeMap.put(12, "Value_12");
        customTreeMap.put(2, "Value_2");
        customTreeMap.put(6, "Value_6");
        customTreeMap.put(13, "Value_13");
        customTreeMap.put(14, "Value_14");

        customTreeMap.put(14, "Value_14");
        // Т.к элемент с таким же ключом существует
        // он не будет добавлен в коллекцию

        Assert.assertEquals(10, customTreeMap.size());
    }

    /**
     * Реализация теста по методу isEmpty.
     * Возвращает булевое значение (true or false)
     */
    @Test
    public void isEmpty() {
        Assert.assertTrue(customTreeMap.isEmpty());

        for (int i = 1; i <= 50; i++) {
            customTreeMap.put(i, "Value_" + i);
        }
        Assert.assertFalse(customTreeMap.isEmpty());
    }

    /**
     * Реализация теста по методу get
     * на получение значения по заданному ключу
     */
    @Test
    public void get() {
        for (int i = 1; i <= 50; i++) {
            customTreeMap.put(i, "Value_" + i);
        }

        Assert.assertEquals(null, customTreeMap.get(-1));
        Assert.assertEquals("Value_33", customTreeMap.get(33));
        Assert.assertEquals(null, customTreeMap.get(51));
    }

    /**
     * Реализация теста по методу put
     * на добавление элемента <K, V>
     */
    @Test
    public void put() {
        Assert.assertEquals(0, customTreeMap.keys().length);
        Assert.assertEquals(0, customTreeMap.values().length);

        Assert.assertNull(customTreeMap.put(0, "Value_0"));
        Assert.assertNull(customTreeMap.put(4, "Value_4"));
        Assert.assertNull(customTreeMap.put(2, "Value_2"));
        Assert.assertNull(customTreeMap.put(-1, "Value_-1"));
        Assert.assertNull(customTreeMap.put(10, "Value_10"));
        Assert.assertNull(customTreeMap.put(15, "Value_15"));

        Assert.assertEquals(Arrays.asList(-1, 0, 2, 4, 10, 15),
                Arrays.asList(customTreeMap.keys()));

        Assert.assertEquals(Arrays.asList("Value_-1", "Value_0", "Value_2",
                "Value_4", "Value_10", "Value_15"), Arrays.asList(customTreeMap.values()));
    }

    /**
     * Реализация теста по методу remove
     * на удаление элемента по ключу
     */
    @Test
    public void remove() {
        Assert.assertTrue(customTreeMap.isEmpty());

        Assert.assertNull(customTreeMap.put(0, "Value_0"));
        Assert.assertNull(customTreeMap.put(2, "Value_2"));
        Assert.assertNull(customTreeMap.put(3, "Value_3"));
        Assert.assertNull(customTreeMap.put(4, "Value_4"));
        Assert.assertNull(customTreeMap.put(5, "Value_5"));
        Assert.assertNull(customTreeMap.put(1, "Value_1"));
        Assert.assertNull(customTreeMap.put(8, "Value_8"));
        Assert.assertNull(customTreeMap.put(6, "Value_6"));
        Assert.assertNull(customTreeMap.put(15, "Value_15"));
        Assert.assertNull(customTreeMap.put(24, "Value_24"));

        Assert.assertEquals(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 8, 15, 24),
                Arrays.asList(customTreeMap.keys()));
        Assert.assertEquals(Arrays.asList("Value_0", "Value_1", "Value_2", "Value_3",
                        "Value_4", "Value_5", "Value_6", "Value_8", "Value_15", "Value_24"),
                Arrays.asList(customTreeMap.values()));

        customTreeMap.remove(0);
        customTreeMap.remove(15);
        customTreeMap.remove(4);

        Assert.assertEquals(Arrays.asList(1, 2, 3, 5, 6, 8, 24),
                Arrays.asList(customTreeMap.keys()));
        Assert.assertEquals(Arrays.asList("Value_1", "Value_2", "Value_3",
                        "Value_5", "Value_6", "Value_8", "Value_24"),
                Arrays.asList(customTreeMap.values()));
    }

    /**
     * Реализация теста для возвращения булевого
     * значения, содержится ли данный Ключ в коллекции или нет
     * (true or false)
     */
    @Test
    public void containsKey() {
        Assert.assertFalse(customTreeMap.containsKey(10));

        Assert.assertNull(customTreeMap.put(0, "Value_0"));
        Assert.assertNull(customTreeMap.put(4, "Value_4"));
        Assert.assertNull(customTreeMap.put(2, "Value_2"));
        Assert.assertNull(customTreeMap.put(-1, "Value_-1"));
        Assert.assertNull(customTreeMap.put(10, "Value_10"));
        Assert.assertNull(customTreeMap.put(15, "Value_15"));

        Assert.assertTrue(customTreeMap.containsKey(10));
        Assert.assertTrue(customTreeMap.containsKey(0));
        Assert.assertFalse(customTreeMap.containsKey(28));
    }

    /**
     * Реализация теста для возвращения булевого
     * значения, содержится ли данное Значение в коллекции или нет
     * (true or false)
     */
    @Test
    public void containsValue() {
        Assert.assertFalse(customTreeMap.containsValue("Value_4"));

        Assert.assertNull(customTreeMap.put(0, "Value_0"));
        Assert.assertNull(customTreeMap.put(4, "Value_4"));
        Assert.assertNull(customTreeMap.put(2, "Value_2"));
        Assert.assertNull(customTreeMap.put(-1, "Value_-1"));
        Assert.assertNull(customTreeMap.put(10, "Value_10"));
        Assert.assertNull(customTreeMap.put(15, "Value_15"));

        Assert.assertTrue(customTreeMap.containsValue("Value_4"));
        Assert.assertTrue(customTreeMap.containsValue("Value_2"));
        Assert.assertFalse(customTreeMap.containsValue("Value_8"));
    }

    /**
     * Реализация метода keys для получения в виде
     * массива всех ключей
     */
    @Test
    public void keys() {
        Assert.assertNull(customTreeMap.put(0, "Value_0"));
        Assert.assertNull(customTreeMap.put(4, "Value_4"));
        Assert.assertNull(customTreeMap.put(2, "Value_2"));
        Assert.assertNull(customTreeMap.put(-1, "Value_-1"));
        Assert.assertNull(customTreeMap.put(10, "Value_10"));
        Assert.assertNull(customTreeMap.put(15, "Value_15"));

        Assert.assertEquals("[-1, 0, 2, 4, 10, 15]",
                Arrays.toString(customTreeMap.keys()));
    }

    /**
     * Реализация метода keys для получения в виде
     * массива всех ключей
     */
    @Test
    public void values() {
        Assert.assertNull(customTreeMap.put(0, "Value_0"));
        Assert.assertNull(customTreeMap.put(4, "Value_4"));
        Assert.assertNull(customTreeMap.put(2, "Value_2"));
        Assert.assertNull(customTreeMap.put(-1, "Value_-1"));
        Assert.assertNull(customTreeMap.put(10, "Value_10"));
        Assert.assertNull(customTreeMap.put(15, "Value_15"));

        Assert.assertEquals("[Value_-1, Value_0, Value_2, Value_4, Value_10, Value_15]",
                Arrays.toString(customTreeMap.values()));
    }
}