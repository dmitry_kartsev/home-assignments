<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <body>
        <h2>Калькулятор доходности вклада</h2><br>
        <form method="POST" action="/finance">
            <table>
                <tr>
                    <td>Сумма на момент открытия</td>
                    <td><input name="sum" type="number" required size=10</td>
                </tr>
                <tr>
                    <td>Процентная ставка</td>
                    <td><input name="percentage" type="number" step="0.1" required size=10</td>
                </tr>
                <tr>
                    <td>Количество лет</td>
                    <td><input name="years" type="number" required size=10</td>
                </tr>
            </table>
            <br>
            <input type="submit" name="result" value=" Посчитать ">
        </form>
    </body>
</html>