<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
    <body>
        <c:choose>
            <c:when test="${param.sum >= 0 && param.sum < 50000}">
                    <p><h2>Ошибка</h2></p>
                    Минимальная сумма на момент<br>
                    открытия вклада 50 000
            </c:when>
            <c:when test="${param.sum < 0 || param.percentage <= 0.0 || param.years <= 0}">
                         <p><h2>Результат</h2></p>
                         Неверный формат данных.<br>
                         Скорректируйте значения
            </c:when>
            <c:when test="${param.sum >= 50000}">
                         <p><h2>Результат</h2></p>
                         Итоговая сумма: ${param.sum + param.sum * (param.percentage / 100) * param.years}
            </c:when>
            </c:choose>
    </body>
</html>