package ru.sberbank.jd.lesson07;

import java.util.Comparator;

public class CustomDigitComparator implements Comparator<Integer> {

    /**
     * Создаем свой компаратор, который
     * сортирует коллекцию Integer
     * (отличную от null)
     * сначала четные, затем нечетные.
     */
    @Override
    public int compare(Integer lhs, Integer rhs) throws NullPointerException {
        if (lhs == null || rhs == null) {
            throw new NullPointerException();
        }

        boolean leftIsEven = lhs % 2 == 0;
        boolean rightIsEven = rhs % 2 == 0;

        if (leftIsEven && rightIsEven || !leftIsEven && !rightIsEven) {
            return 0;
        } else if (leftIsEven) {
            return -1;
        } else {
            return 1;
        }
    }
}