package ru.sberbank.jd.lesson07;

import java.util.Objects;

/**
 * Создаем класс Person реализующий
 * функциональный интерфейс Comparable
 */
public class Person implements Comparable<Person> {

    /**
     * Создаем необходимые поля класса
     */
    private final String name;
    private final String city;
    private final int age;

    /**
     * Создаем конструктор
     */
    public Person(String name, String city, int age) {
        this.name = name;
        this.city = city;
        this.age = age;
    }

    /**
     * Переопределяем метод equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }

        Person other = (Person) obj;

        return name.equalsIgnoreCase(other.name) &&
                city.equalsIgnoreCase(other.city) && age == other.age;
    }

    /**
     * Переопределяем метод hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(name.toLowerCase(), city.toLowerCase(), age);
    }

    /**
     * Переопределяем метод compareTo
     * (Для сортировки по полю city и полю name
     * в соответствии с заданием)
     */
    @Override
    public int compareTo(Person person) {
        int cityResult = this.city.compareToIgnoreCase(person.city);
        if (cityResult == 0) {
            return this.name.compareToIgnoreCase(person.name);
        } else {
            return cityResult;
        }
    }

    /**
     * Переопределяем метод toString
     */
    @Override
    public String toString() {
        return "Person {" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}