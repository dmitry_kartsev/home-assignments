package ru.sberbank.jd.lesson07;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CustomDigitComparatorTest {

    /**
     * Тестируем созданный компаратор на соритровку
     * с разным набором параметров
     */
    @Test
    public void compareByOddInteger() {
        CustomDigitComparator myComparator = new CustomDigitComparator();

        Assert.assertEquals(-1, myComparator.compare(2, -1));
        Assert.assertEquals(0, myComparator.compare(0, 2));
        Assert.assertEquals(0, myComparator.compare(1, 3));
        Assert.assertEquals(1, myComparator.compare(-1, 2));
        Assert.assertEquals(-1, myComparator.compare(8, -3));
        Assert.assertEquals(1, myComparator.compare(3, 14));
        Assert.assertEquals(0, myComparator.compare(3, 3));
        Assert.assertEquals(1, myComparator.compare(-1, -8));
        Assert.assertEquals(0, myComparator.compare(2, 4));
        Assert.assertEquals(0, myComparator.compare(-1, -3));
    }

    /**
     * Тестируем созданный компаратор на соритровку
     * коллекции по заданному условию при добавлении
     * в коллекцию "null" и запуск компаратора
     */
    @Test(expected = NullPointerException.class)
    public void compareByNullException() {
        List<Integer> list = new ArrayList<>();
        for (int i = -2; i <= 15; i++) {
            list.add(i);
        }

        list.add(null);

        Comparator<Integer> cmpByEven = new CustomDigitComparator();
        list.sort(cmpByEven);
    }
}