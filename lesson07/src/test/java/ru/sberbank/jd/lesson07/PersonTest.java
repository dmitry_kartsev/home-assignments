package ru.sberbank.jd.lesson07;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonTest {

    /**
     * Реализуем тест на переопределенные методы
     * equals и hashCode
     */
    @Test
    public void equalsAndHashCode() {
        Person person1 = new Person("DmitrY", "MoScow", 29);
        Person person2 = new Person("dmitRy", "moscoW", 29);

        Assert.assertEquals(person1, person2);

        int a = person1.hashCode();
        int b = person2.hashCode();

        Assert.assertEquals(a, b);
    }

    /**
     * Реализуем тест на переопределенный метод
     * compareTo (сортировка по полю city и полю name)
     */
    @Test
    public void compareToByCity() {
        List<Person> list = new ArrayList<>(Arrays.asList(
                new Person("John", "New-York", 36),
                new Person("Bob", "Alabama", 19),
                new Person("Alex", "Kansas", 25),
                new Person("Rob", "Detroit", 18),
                new Person("Steve", "San-Francisco", 29)
        ));

        Assert.assertEquals(3, list.get(0).
                compareTo(new Person("Alex", "Kansas", 25)));
        Assert.assertEquals(0, list.get(1).
                compareTo(new Person("Bob", "Alabama", 19)));
        Assert.assertEquals(-8, list.get(2).
                compareTo(new Person("Steve", "San-Francisco", 29)));
        Assert.assertEquals(-19, list.get(3).
                compareTo(new Person("John", "Washington", 37)));
        Assert.assertEquals(17, list.get(4).
                compareTo(new Person("Rob", "Boston", 29)));
    }
}