package ru.sberbank.jd.lesson02;

import org.junit.Test;

import static org.junit.Assert.*;

public class NodImplTest {

    Nod nod = new NodImpl();

    /**
     * Проверяем выполнение теста на нахождение НОД двух целых чисел
     * (вводим ожидаемое число и два целых числа, между которыми ищем НОД)
     */
    @Test
    public void calculate() {
        assertEquals(2, nod.calculate(32, 50));
        assertEquals(8, nod.calculate(152, 40));
        assertEquals(32, nod.calculate(96, 128));
        assertEquals(6, nod.calculate(72, 174));
        assertEquals(4, nod.calculate(20, 36));
    }

}