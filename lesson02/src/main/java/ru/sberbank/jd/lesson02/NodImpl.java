package ru.sberbank.jd.lesson02;

public class NodImpl implements Nod {

    /**
     * Решение Алгоритмом Евклида
     * Создаем цикл с условием (пока остаток от деления по модулю "%" не равен нулю)
     * Создаем дополниельную переменную tmp
     * (для хранения промежуточного результата остатка от деления по модулю).
     * <p>
     * В случае, если  first > second, то выполняется first = first % second, если first меньше,
     * то выполняется second = second % first, в итоге, таким образом мы находим вначале остаток от деления,
     * а затем повторяем цикл.
     * <p>
     * До тех пор, пока Остаток от деления будет == 0
     * <p>
     * Затем наш цикл прекращается и получаем НОД.
     * Одно из числел будет == 0, поэтому возвращаем одно число.
     */
    @Override
    public int calculate(int first, int second) {
        while (first != 0 && second != 0) {
            int tmp = first % second;
            first = second;
            second = tmp;
        }
        return first;
    }
}

