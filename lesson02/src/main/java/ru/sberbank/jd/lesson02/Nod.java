package ru.sberbank.jd.lesson02;

public interface Nod {

  /**
   * вычислить наибольший общий делитель двух целых числел
   * @param first первое число
   * @param second второе число
   * @return наибольший общий делитель
   */
  int calculate(int first, int second);
}
