package ru.sberbank.jd.lesson10;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Assert;
import org.junit.Test;
import ru.sberbank.jd.lesson10.input.Catalog;
import ru.sberbank.jd.lesson10.output.Album;
import ru.sberbank.jd.lesson10.output.Artist;
import ru.sberbank.jd.lesson10.output.Country;
import ru.sberbank.jd.lesson10.output.Registry;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MainTest {
    private static final String FILE_INPUT_PATH = "./src/test/resources/input/fileTest.xml";

    /**
     * Тест на чтение (из .xml) и запись (в .json)
     * тестового файла
     */
    @Test
    public void convertToJson() throws IOException {
        String file_output_path = "./src/test/resources/output/newFileTest.json";

        /**
         * Считываем тестовый файл формата .xml
         * и сохраняем в формат .json
         */
        XmlMapper xmlMapper = new XmlMapper();
        ObjectMapper jsonMapper = new ObjectMapper();
        String content = new String(Files.readAllBytes(Paths.get(FILE_INPUT_PATH)));

        Catalog catalog = xmlMapper.readValue(content, Catalog.class);
        Registry registry = Converter.convert(catalog);

        try (FileOutputStream fos = new FileOutputStream(file_output_path);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            String xmlStr = jsonMapper.writeValueAsString(registry);
            osw.write(xmlStr);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        /**
         * Считываем сохраненный файл формата (.json)
         */
        FileInputStream fis = new FileInputStream(file_output_path);
        ObjectMapper mapper = new ObjectMapper();
        Registry registryTestFile = mapper.readValue(fis, Registry.class);

        /**
         * Проверка Значений из записанного файла
         */
        Country country = registryTestFile.getCountries().get(0);
        Assert.assertEquals(1, registryTestFile.getCountries().size());
        Assert.assertEquals("USA", country.getName());

        Artist artist = country.getArtists().get(0);
        Assert.assertEquals(1, country.getArtists().size());
        Assert.assertEquals("Will Smith", artist.getName());

        Album album = artist.getAlbums().get(0);
        Assert.assertEquals(1, artist.getAlbums().size());
        Assert.assertEquals("Big Willie style", album.getName());
        Assert.assertEquals(1997, album.getYear());

        fis.close();
    }

    /**
     * Тест на чтение (из .xml) и запись (в .serialize)
     * тестового файла
     */
    @Test
    public void convertToSerialize() throws IOException, ClassNotFoundException {
        String file_output_path = "./src/test/resources/output/newFileTest.serialize";

        /**
         * Считываем тестовый файл формата .xml
         * и сохраняем в .serialize
         */
        XmlMapper xmlMapper = new XmlMapper();
        String content = new String(Files.readAllBytes(Paths.get(FILE_INPUT_PATH)));

        Catalog catalog = xmlMapper.readValue(content, Catalog.class);
        Registry registry = Converter.convert(catalog);

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file_output_path))) {
            oos.writeObject(registry);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        /**
         * Считываем сохраненный файл формата (.serialize)
         */
        FileInputStream fis = new FileInputStream(file_output_path);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Registry reg = (Registry) ois.readObject();

        /**
         * Проверка Значений из записанного файла
         */
        Country country = reg.getCountries().get(0);
        Assert.assertEquals(1, reg.getCountries().size());
        Assert.assertEquals("USA", country.getName());

        Artist artist = country.getArtists().get(0);
        Assert.assertEquals(1, country.getArtists().size());
        Assert.assertEquals("Will Smith", artist.getName());

        Album album = artist.getAlbums().get(0);
        Assert.assertEquals(1, artist.getAlbums().size());
        Assert.assertEquals("Big Willie style", album.getName());
        Assert.assertEquals(1997, album.getYear());

        ois.close();
        fis.close();
    }

    /**
     * Тест на чтение (из .xml) и перезапись (в .xml)
     * тестового файла с изменениями в соответствии с заданием
     */
    @Test
    public void convertToXml() throws IOException {
        String file_output_path = "./src/test/resources/output/newFileTest.xml";

        /**
         * Считываем тестовый файл формата .xml
         * и сохраняем в измененный .xml
         */
        XmlMapper xmlMapper = new XmlMapper();
        String content = new String(Files.readAllBytes(Paths.get(FILE_INPUT_PATH)));

        Catalog catalog = xmlMapper.readValue(content, Catalog.class);
        Registry registry = Converter.convert(catalog);

        try (FileOutputStream fos = new FileOutputStream(file_output_path);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            String xmlStr = xmlMapper.writeValueAsString(registry);

            osw.write(xmlStr);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        /**
         * Считываем сохраненный файл формата (.xml)
         */
        FileInputStream fis = new FileInputStream(file_output_path);
        XmlMapper mapper = new XmlMapper();
        Registry reg = mapper.readValue(fis, Registry.class);

        /**
         * Проверка Значений из записанного файла
         */
        Country country = reg.getCountries().get(0);
        Assert.assertEquals(1, reg.getCountries().size());
        Assert.assertEquals("USA", country.getName());

        Artist artist = country.getArtists().get(0);
        Assert.assertEquals(1, country.getArtists().size());
        Assert.assertEquals("Will Smith", artist.getName());

        Album album = artist.getAlbums().get(0);
        Assert.assertEquals(1, artist.getAlbums().size());
        Assert.assertEquals("Big Willie style", album.getName());
        Assert.assertEquals(1997, album.getYear());

        fis.close();
    }
}