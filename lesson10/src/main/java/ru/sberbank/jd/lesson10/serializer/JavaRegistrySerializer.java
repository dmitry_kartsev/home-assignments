package ru.sberbank.jd.lesson10.serializer;

import ru.sberbank.jd.lesson10.output.Registry;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class JavaRegistrySerializer implements RegistrySerializer, Serializable {
    public static final String PATH = "./lesson10/src/main/resources/output/artist_by_country.serialized";

    /**
     * Переопределяем метод для сериализации
     * и записи в файл c расширением .serialize
     */
    @Override
    public void serializeAndWrite(Registry registry) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(PATH))) {
            oos.writeObject(registry);
            System.out.println("File has been written in the format: .serialize");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}