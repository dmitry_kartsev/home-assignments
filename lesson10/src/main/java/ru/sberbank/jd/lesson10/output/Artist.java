package ru.sberbank.jd.lesson10.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {

    @JsonProperty("name")
    @JacksonXmlProperty(localName = "Name")
    private String name;

    @JsonProperty("albums")
    @JacksonXmlProperty(localName = "Album")
    @JacksonXmlElementWrapper(localName = "Albums")
    private List<Album> albums;

    public Artist() {
        albums = new ArrayList<>();
    }

    public Artist(String name, List<Album> albums) {
        this.name = name;
        this.albums = albums;
    }

    public String getName() {
        return name;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "name='" + name + '\'' +
                ", albums=" + albums +
                '}';
    }
}