package ru.sberbank.jd.lesson10;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.sberbank.jd.lesson10.input.Catalog;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CatalogProvider {
    private static final String CATALOG_PATH = "./lesson10/src/main/resources/input/cd_catalog.xml";

    /**
     * Метод для десериализации из файла
     * c расширением .xml
     */
    public static Catalog getCatalog() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        String content = new String(Files.readAllBytes(Paths.get(CATALOG_PATH)));
        return xmlMapper.readValue(content, Catalog.class);
    }
}