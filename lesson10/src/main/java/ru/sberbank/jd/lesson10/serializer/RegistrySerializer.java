package ru.sberbank.jd.lesson10.serializer;

import ru.sberbank.jd.lesson10.output.Registry;

/**
 * Интерфейс для метода сериализации и записи в файл
 */
@FunctionalInterface
public interface RegistrySerializer {
    void serializeAndWrite(Registry registry);
}