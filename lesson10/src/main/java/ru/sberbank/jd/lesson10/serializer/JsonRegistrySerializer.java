package ru.sberbank.jd.lesson10.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.sberbank.jd.lesson10.output.Registry;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class JsonRegistrySerializer implements RegistrySerializer {
    private static final ObjectMapper jsonMapper = new ObjectMapper();
    private static final String PATH = "./lesson10/src/main/resources/output/artist_by_country.json";

    /**
     * Переопределяем метод для сериализации
     * и записи в файл c расширением .json
     */
    @Override
    public void serializeAndWrite(Registry registry) {
        try (FileOutputStream fos = new FileOutputStream(PATH);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            String xmlStr = jsonMapper.writeValueAsString(registry);
            System.out.println("File has been written in the format: .json");
            osw.write(xmlStr);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}