package ru.sberbank.jd.lesson10.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;

@JacksonXmlRootElement(localName = "Album")
public class Album implements Serializable {

    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    @JsonProperty("year")
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private int year;

    public Album() {
    }

    public Album(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}