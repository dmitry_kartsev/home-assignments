package ru.sberbank.jd.lesson10;

import ru.sberbank.jd.lesson10.input.Catalog;
import ru.sberbank.jd.lesson10.output.Registry;
import ru.sberbank.jd.lesson10.serializer.JavaRegistrySerializer;
import ru.sberbank.jd.lesson10.serializer.JsonRegistrySerializer;
import ru.sberbank.jd.lesson10.serializer.RegistrySerializer;
import ru.sberbank.jd.lesson10.serializer.XmlRegistrySerializer;

import java.io.IOException;

public class Main {

    /**
     * Главный метод для сериализации в 3 данных файла
     * .json, .serialize, .xml
     */
    public static void main(String[] args) {
        RegistrySerializer javaSerializer = new JavaRegistrySerializer();
        RegistrySerializer xmlSerializer = new XmlRegistrySerializer();
        RegistrySerializer jsonSerializer = new JsonRegistrySerializer();

        try {
            Catalog catalog = CatalogProvider.getCatalog();
            Registry registry = Converter.convert(catalog);

            javaSerializer.serializeAndWrite(registry);
            xmlSerializer.serializeAndWrite(registry);
            jsonSerializer.serializeAndWrite(registry);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}