package ru.sberbank.jd.lesson10.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Country")
public class Country implements Serializable {

    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    @JsonProperty("artists")
    @JacksonXmlProperty(localName = "Artist")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Artist> artists = new ArrayList<>();

    public Country() {
    }

    public Country(String name, List<Artist> artists) {
        this.name = name;
        this.artists = artists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", artists=" + artists +
                '}';
    }
}