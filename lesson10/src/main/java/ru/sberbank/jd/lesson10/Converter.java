package ru.sberbank.jd.lesson10;

import ru.sberbank.jd.lesson10.input.Catalog;
import ru.sberbank.jd.lesson10.input.Cd;
import ru.sberbank.jd.lesson10.output.Album;
import ru.sberbank.jd.lesson10.output.Artist;
import ru.sberbank.jd.lesson10.output.Country;
import ru.sberbank.jd.lesson10.output.Registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Converter {
    /**
     * Метод для конвертации объектов из файла .xml
     */
    public static Registry convert(Catalog catalog) {
        List<Cd> cds = catalog.getCds();

        Map<String, List<Cd>> countryCd = cds.stream()
                .collect(Collectors.groupingBy(Cd::getCountry));

        List<Country> countries = new ArrayList<>();

        for (String country : countryCd.keySet()) {

            Map<String, List<Cd>> artistCd = countryCd.get(country).stream()
                    .collect(Collectors.groupingBy(Cd::getArtist));

            ArrayList<Artist> artists = new ArrayList<>();

            for (String artist : artistCd.keySet()) {
                List<Album> albums = artistCd.get(artist).stream()
                        .map(c -> new Album(c.getTitle(),
                                c.getYear())).collect(Collectors.toList());

                artists.add(new Artist(artist, albums));
            }
            countries.add(new Country(country, artists));
        }
        return new Registry(countries);
    }
}