package ru.sberbank.jd.lesson10.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    @JsonProperty("countryCount")
    @JacksonXmlProperty(localName = "countryCount", isAttribute = true)
    private int countryCount;

    @JsonProperty("countries")
    @JacksonXmlProperty(localName = "Country")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Country> countries = new ArrayList<>();

    public Registry() {
        countries = new ArrayList<>();
        countryCount = countries.size();
    }

    public Registry(List<Country> countries) {
        this.countries = countries;
        this.countryCount = countries.size();
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public String toString() {
        return "Registry{" +
                "countries=" + countries +
                '}';
    }
}