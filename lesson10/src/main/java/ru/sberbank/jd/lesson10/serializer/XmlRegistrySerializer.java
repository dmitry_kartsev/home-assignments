package ru.sberbank.jd.lesson10.serializer;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.sberbank.jd.lesson10.output.Registry;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class XmlRegistrySerializer implements RegistrySerializer {
    private static final XmlMapper xmlMapper = new XmlMapper();
    private static final String PATH = "./lesson10/src/main/resources/output/artist_by_country.xml";

    /**
     * Переопределяем метод для сериализации
     * и записи в файл c расширением .xml
     */
    @Override
    public void serializeAndWrite(Registry registry) {
        try (FileOutputStream fos = new FileOutputStream(PATH);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            String xmlStr = xmlMapper.writeValueAsString(registry);
            System.out.println("File has been written in the format: .xml");
            osw.write(xmlStr);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}