package ru.sberbank.jd.lesson06;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class MyCustomArrayTest {

    /**
     * Реализация теста на вычисление колличества
     * элеметнов в динамическом массиве
     */
    @Test
    public void size() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();

        Assert.assertEquals(0, array.size());

        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertEquals(3, array.size());
    }

    /**
     * Реализация теста на вычисление
     * пуст ли динамический массив или нет
     */
    @Test
    public void isEmpty() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();

        Assert.assertTrue(array.isEmpty());

        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertFalse(array.isEmpty());
    }

    /**
     * Реализация теста на добавление элемента
     * в созданный динамический массив и автоувеличение
     * длины массива
     */
    @Test
    public void add() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertEquals(3, array.size());

        array.add("four");
        array.add("five");
        array.add("six");
        array.add("seven");
        array.add("eight");
        array.add("nine");
        array.add("ten");
        array.add("eleven");
        array.add("twelve");

        Assert.assertEquals(12, array.size());
        Assert.assertEquals(20, array.getCapacity());
    }

    /**
     * Реализация теста на добавление массива
     * в созданный динамический массив
     */
    @Test
    public void addAll_Array() {
        CustomArrayImpl<Integer> array1 = new CustomArrayImpl<>();
        array1.add(1);
        array1.add(2);
        array1.add(3);
        array1.add(4);
        array1.add(5);
        array1.add(6);
        array1.add(7);
        array1.add(8);
        array1.add(9);

        Integer[] array2 = {10, 11, 12};

        Assert.assertTrue(array1.addAll(array2));

        Assert.assertEquals(12, array1.size());
        Assert.assertEquals(20, array1.getCapacity());
    }

    /**
     * Реализация теста на добавление коллекции
     * в созданный динамический массив
     */
    @Test
    public void addAllWithCollection() {
        List<String> listArray = new LinkedList<>();

        listArray.add("Java");
        listArray.add("World");

        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("Hello");

        array.addAll(listArray);

        Assert.assertEquals(3, array.size());
        Assert.assertEquals("[Hello, Java, World]", Arrays.toString(array.toArray()));
    }

    /**
     * Реализация теста на добавление массива
     * в созданный динамический массив по индексу
     */
    @Test
    public void addAll_With_Index_And_T() {
        String[] array1 = {"Hello", "Hi", "Hey", "World"};

        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("Java");
        array.add("World");

        // Добавление в индекс №1
        array.addAll(1, array1);
        Assert.assertEquals("[Java, Hello, Hi, Hey, World, World]",
                Arrays.toString(array.toArray()));
        Assert.assertEquals(6, array.size());

        String[] array2 = {"My", "Custom", "Array"};
        // Добавление в индекс №2
        array.addAll(2, array2);

        Assert.assertEquals("[Java, Hello, My, Custom, " +
                        "Array, Hi, Hey, World, World]",
                Arrays.toString(array.toArray()));
        Assert.assertEquals(9, array.size());


        String[] array3 = {"Sunday"};
        // Добавление в индекс №5
        array.addAll(5, array3);

        Assert.assertEquals("[Java, Hello, My, Custom, " +
                        "Array, Sunday, Hi, Hey, World, World]",
                Arrays.toString(array.toArray()));
        Assert.assertEquals(10, array.size());
    }

    /**
     * Реализация теста на добавление массива
     * в созданный динамический массив по
     * несуществующему индексу
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void addAll_With_Wrong_Index_And_T() {
        String[] array1 = {"Hello", "Sunday"};

        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("Morning");
        array.add("World");

        // Добавление в несуществующий индекс №3
        array.addAll(3, array1);

        Assert.assertEquals(2, array.size());
    }

    /**
     * Реализация теста на получение элемента
     * в динамическом массиве по индексу
     */
    @Test
    public void getByIndex() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertEquals("three", array.get(2));
    }

    /**
     * Реализация теста на получение элемента
     * в динамическом массиве по несуществующему индексу
     */
    @Test(expected = RuntimeException.class)
    public void getRuntimeException() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");

        array.get(11);
    }

    /**
     * Реализация теста на назначение элемента
     * в динамическом массиве по индексу
     */
    @Test
    public void set() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");
        array.add("four");
        array.add("five");

        Assert.assertEquals("[one, two, three, four, five]",
                Arrays.toString(array.toArray()));

        Assert.assertEquals(("three"), array.set(2, "seven"));

        Assert.assertEquals("[one, two, seven, four, five]",
                Arrays.toString(array.toArray()));
    }

    /**
     * Реализация теста на удаление (через индекс элемента) и сдвиг
     * остальных элементов массива на удаленный элемент
     */
    @Test
    public void remove_ByIndex() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertEquals(3, array.size());
        array.remove(1);
        Assert.assertEquals(2, array.size());
        Assert.assertEquals("[one, three]", Arrays.toString(array.toArray()));
    }

    /**
     * Реализация теста на удаление (через элемент) и сдвиг
     * остальных элементов массива на удаленный элемент
     */
    @Test
    public void remove_By_T_item() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertEquals(3, array.size());

        Assert.assertTrue(array.remove("two"));
        Assert.assertFalse(array.remove("four"));
        Assert.assertEquals(2, array.size());
    }

    /**
     * Реализация теста на проверку элемента
     * существует ли он (true) или нет (false)
     * в динамическом масиве
     */
    @Test
    public void contains() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertTrue(array.contains("three"));
        Assert.assertFalse(array.contains("four"));
    }

    /**
     * Реализация теста на получение индекса по элементу
     * в динамичеком массиве
     */
    @Test
    public void indexOf() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");

        Assert.assertEquals(0, array.indexOf("one"));
        Assert.assertEquals(1, array.indexOf("two"));
        Assert.assertEquals(2, array.indexOf("three"));

        Assert.assertEquals(-1, array.indexOf("five"));
    }

    /**
     * Реализация теста на увеличение размера внутреннего массива,
     */
    @Test
    public void ensureCapacity() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");
        array.add("four");
        array.add("five");

        Assert.assertEquals(10, array.getCapacity());

        array.ensureCapacity(100);

        Assert.assertEquals(100, array.getCapacity());
    }

    /**
     * Реализация теста на получение
     * текущего размера внутреннего массива
     */
    @Test
    public void getCapacity() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");
        array.add("four");
        array.add("five");

        Assert.assertEquals(10, array.getCapacity());
    }

    /**
     * Реализация теста на возврат динамического массива
     * с элементами в обратном порядке
     */
    @Test
    public void reverse() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("one");
        array.add("two");
        array.add("three");
        array.add("four");
        array.add("five");

        Assert.assertEquals("[one, two, three, four, five]", Arrays.toString(array.toArray()));
        array.reverse();
        Assert.assertEquals("[five, four, three, two, one]", Arrays.toString(array.toArray()));
    }

    /**
     * Реализация теста на преобразование
     * списка элементов в массив
     */
    @Test
    public void toArray() {
        CustomArrayImpl<String> array = new CustomArrayImpl<>();
        array.add("A");
        array.add("B");
        array.add("C");

        Assert.assertEquals(Arrays.asList("A", "B", "C"), Arrays.asList(array.toArray()));
    }
}