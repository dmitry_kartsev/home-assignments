package ru.sberbank.jd.lesson06;

import java.util.Arrays;
import java.util.Collection;


public class CustomArrayImpl<T> implements CustomArray<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private T[] array;
    private int size;
    private int capacity;


    /**
     * Конструктор без параметров
     */
    public CustomArrayImpl() {
        this.array = (T[]) new Object[DEFAULT_CAPACITY];
        this.capacity = array.length;
    }

    /**
     * Конструктор c начальным заданным параметром
     * длины массива
     */
    public CustomArrayImpl(int capacity) {
        if (capacity > 0) {
            this.array = (T[]) new Object[capacity];
        } else if (capacity == 0) {
            this.array = (T[]) new Object[DEFAULT_CAPACITY];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " +
                    capacity);
        }
        this.capacity = array.length;
    }

    /**
     * Конструктор c заданным экземпляром коллекции
     */
    public CustomArrayImpl(Collection<T> c) {
        this();// Используется ссылка на созданный конструктор без параметров
        // как я понял из источников про ключевое слово this
        if (c == null) {
            throw new IllegalArgumentException("Collection is null.");
        }
        this.array = (T[]) c.toArray();
        this.size = c.size();
    }

    /**
     * Метод на определение количества
     * элементов в динамическом массиве
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Метод на определение пуст ли
     * динамический массив или нет
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Метод на добавление элементов
     * в динамическом массиве
     */
    @Override
    public boolean add(T item) {
        if (size == capacity) {
            ensureCapacity(capacity * 2);
        }

        array[size] = item;
        size++;
        return true;
    }

    /**
     * Метод на добавление массива элементов
     * в динамический массив
     */
    @Override
    public boolean addAll(T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter items is null");
        }

        for (T t : items) {
            add(t);
        }
        return true;
    }

    /**
     * Метод на добавление коллекции элементов
     * в динамический массив
     */
    @Override
    public boolean addAll(Collection<T> items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter items is null");
        }
        return this.addAll((T[]) items.toArray());
    }

    /**
     * Метод на добавление массива
     * в созданный динамический массив
     */
    @Override
    public boolean addAll(int index, T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter items is null");
        }
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("Illegal index " + index);
        }

        int itemsLength = items.length;

        if (this.size + itemsLength > this.capacity) {
            this.capacity = itemsLength;
            this.array = (T[]) new Object[capacity];
            Arrays.copyOf(array, capacity);
        }

        int newPotentionPosition = index + itemsLength;
        int copyLength = this.size - index;
        System.arraycopy(this.array, index, this.array, newPotentionPosition, copyLength);

        for (int i = 0; i < itemsLength; i++) {
            int actualIndex = index + i;
            this.array[actualIndex] = items[i];
        }

        this.size += itemsLength;
        return true;
    }


    /**
     * Метод на получение элемента
     * в динамическом массиве по индексу
     */
    @Override
    public T get(int index) {
        if (index >= this.size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Wrong index = " + index);
        }
        return array[index];
    }

    /**
     * Метод на назначение элемента
     * в динамическом массиве по индексу
     */
    @Override
    public T set(int index, T item) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("No such index exists: " + index);
        }
        T oldValue = this.array[index];
        this.array[index] = item;
        return oldValue;
    }


    /**
     * Метод на удаление элемента
     * в динамическом массиве по индексу
     * и сдвиг остальных элементов в сторону удаленного
     */
    @Override
    public void remove(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("No such index exists: " + index);
        }
        System.arraycopy(this.array, index + 1, this.array, index,
                this.size - index);
        this.size--;
    }

    /**
     * Метод на удаление элемента
     * в динамическом массиве по заданному элементу
     * и сдвиг остальных элементов в сторону удаленного
     */
    @Override
    public boolean remove(T item) {
        int foundIndex = indexOf(item);
        if (foundIndex >= 0) {
            this.remove(foundIndex);
            return true;
        }
        return false;
    }

    /**
     * Метод на вычисление существует ли заданный
     * элемент в динамическом массиве, True or False
     */
    @Override
    public boolean contains(T item) {
        for (int i = 0; i < size; ++i) {
            if (array[i].equals(item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Метод на получение индекса первого вхождения
     * заданного элемента в динамическом массиве
     */
    @Override
    public int indexOf(T item) {
        int index;
        for (index = 0; index < size; index++) {
            if (array[index].equals(item)) {
                return index;
            }
        }
        return -1;
    }

    /**
     * Метод на увеличение размера внутреннего массива,
     * чтобы в него поместилось количество элементов,
     * переданных в "newElementsCount"
     */
    @Override
    public void ensureCapacity(int newElementsCount) {
        if (newElementsCount == this.capacity) {
            return;
        }
        this.capacity = newElementsCount;
        this.array = (T[]) new Object[capacity];
        Arrays.copyOf(array, capacity);
    }

    /**
     * Метод на получение количества элементов
     * динамического массиве
     */
    @Override
    public int getCapacity() {
        return capacity;
    }

    /**
     * Метод на изменение порядка
     * элементов внутреннего массива
     */
    @Override
    public void reverse() {
        for (int i = 0; i < size / 2; i++) {
            T temp = array[i];
            array[i] = array[size - 1 - i];
            array[size - 1 - i] = temp;
        }
    }

    /**
     * Метод на
     * получение копии
     * внутреннего массива
     */
    @Override
    public Object[] toArray() {
        return Arrays.copyOf(array, size);
    }
}