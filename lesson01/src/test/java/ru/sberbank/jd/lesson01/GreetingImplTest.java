package ru.sberbank.jd.lesson01;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;


public class GreetingImplTest {

    Greeting greeting = new GreetingImpl();

    /**
     * Тестируем реализацию метода на получение имени
     */
    @Test
    public void getFirstName() {
        Assert.assertEquals("Dmitry", greeting.getFirstName());
    }

    /**
     * Тестируем реализацию метода на получение отчества
     */
    @Test
    public void getSecondName() {
        Assert.assertEquals("Sergeevich", greeting.getSecondName());
    }

    /**
     * Тестируем реализацию метода на получение фамилии
     */
    @Test
    public void getLastName() {
        Assert.assertEquals("Kartsev", greeting.getLastName());
    }

    /**
     * Тестируем реализацию метода на получение года рождения
     */
    @Test
    public void getBirthYear() {
        Assert.assertEquals("1993", greeting.getBirthYear());
    }

    /**
     * Тестируем реализацию метода на получение списка хобби
     */
    @Test
    public void getHobbies() {
        Assert.assertEquals(new ArrayList<>((Arrays.asList("running", "study",
                "mountain hiking", "drive a car"))), greeting.getHobbies());
    }

    /**
     * Тестируем реализацию метода на получение ссылки на bitbucket.org
     */
    @Test
    public void getBitbucketUrl() {
        Assert.assertEquals("https://bitbucket.org/dmitry_kartsev/home-assignments/src/master/",
                greeting.getBitbucketUrl());
    }

    /**
     * Тестируем реализацию метода на получение личного номера телефона
     */
    @Test
    public void getPhone() {
        Assert.assertEquals("+7 906 764 3258", greeting.getPhone());
    }

    /**
     * Тестируем реализацию метода на получение списка ожиданий от курса
     */
    @Test
    public void getCourseExpectations() {
        Assert.assertEquals(new ArrayList<>((Arrays.asList("Learn how to effectively program in java " +
                "and additional programs"))), greeting.getCourseExpectations());
    }

    /**
     * Тестируем реализацию метода на получение информации об образовании
     */
    @Test
    public void getEducationInfo() {
        Assert.assertEquals(new ArrayList<>((Arrays.asList("MOSCOW STATE UNIVERSITY OF GEODESY AND CARTOGRAPHY",
                "applied geodesy and GNSS specialist"))), greeting.getEducationInfo());
    }
}