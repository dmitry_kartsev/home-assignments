package ru.sberbank.jd.lesson01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class GreetingImpl implements Greeting {

    private String firstName;
    private String secondName;
    private String lastName;
    private String birthYear;
    private Collection<String> hobbies;
    private String bitbucketUrl;
    private String phone;
    private Collection<String> courseExpectations;
    private Collection<String> educationInfo;


    public GreetingImpl () {
        this.firstName = "Dmitry";
        this.secondName = "Sergeevich";
        this.lastName = "Kartsev";
        this.birthYear = "1993";
        this.hobbies = new ArrayList<>((Arrays.asList("running", "study", "mountain hiking","drive a car")));
        this.bitbucketUrl = "https://bitbucket.org/dmitry_kartsev/home-assignments/src/master/";
        this.phone = "+7 906 764 3258";
        this.courseExpectations = new ArrayList<>((Arrays.asList("Learn how to effectively program in java " +
                "and additional programs")));
        this.educationInfo = new ArrayList<>((Arrays.asList("MOSCOW STATE UNIVERSITY OF GEODESY AND CARTOGRAPHY",
                "applied geodesy and GNSS specialist")));
    }

    /**
     * Вывод Имени
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Вывод Отчества
     */
    @Override
    public String getSecondName() {
        return secondName;
    }

    /**
     * Вывод Фамилии
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Выводит год рождения
     */
    @Override
    public String getBirthYear() {
        return birthYear;
    }

    /**
     * Вывод списка хобби
     */
    @Override
    public Collection<String> getHobbies() {
        return hobbies;
    }

    /**
     * Вывод ссылки на репозиторий на bitbucket.org
     */
    @Override
    public String getBitbucketUrl() {
        return bitbucketUrl;
    }

    /**
     * Выводит контактный номер телефона
     */
    @Override
    public String getPhone() {
        return phone;
    }

    /**
     * Вывод ожиданий от курса
     */
    @Override
    public Collection<String> getCourseExpectations() {
        return courseExpectations;
    }

    /**
     * Вывод информации о моем образовании
     */
    @Override
    public Collection<String> getEducationInfo() {
        return educationInfo;
    }
}
