package ru.sberbank.jd.lesson04;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class WordCount {

    public static void main(String[] args) throws IOException {
        WordCount wc = new WordCount();
        wc.analyzeArgs(args);
    }

    public void analyzeArgs(String[] args) throws IOException {
        /**
         * Создаем входящий поток, преобразуем аргументы командной строки
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> strings = Arrays.asList(args);

        /**
         * В случае, если аргументы командной строки отсутствуют:
         * Считываем данные из старндартного ввода и выводим количество строк,
         * слов и символов
         */
        if (strings.isEmpty()) {
            String string = reader.readLine();
            String[] standartInput = string.split(" ");
            System.out.println("\t" + 1 + "\t" + standartInput.length + "\t" + string.length());
            reader.close();
        }

        /**
         * В случае, если число аргументов командной строки = 1:
         * Считываем данные из старндартного ввода и выводим в консоль Help,
         * Version, количество слов и количество символов
         */
        if (strings.size() == 1) {
            String firstArgument = strings.get(0);
            if ("--help".equals(firstArgument)) {
                printHelp();
                return;
            }
            if ("--version".equals(firstArgument)) {
                printVersion();
                return;
            }
            if ("-c".equals(firstArgument) || ("--bytes".equals(firstArgument))) {
                System.out.println("\t" + reader.readLine().length());
            }
            if ("-m".equals(firstArgument) || ("--chars".equals(firstArgument))) {
                System.out.println("\t" + reader.readLine().length());
            }
            if ("-w".equals(firstArgument) || "--words".equals(firstArgument)) {
                String[] standartInput = reader.readLine().split(" ");
                System.out.println("\t" + standartInput.length);
            }
        }

        /**
         * В случае, если число аргументов командной строки больше или равно 2:
         * Считываем данные из файла и выводим данные в соответствии с ключом
         */
        try {
            if (args.length >= 2) {
                String firstArgument = strings.get(0);
                if (firstArgument.equals("-c") || firstArgument.equals("--byte")) {
                    int countByte = 0;
                    for (int i = 1; i < args.length; i++) {
                        System.out.println("\t" + getByteCounts(firstArgument) + " " + args[i]);
                        countByte += getByteCounts(args[i]);
                    }
                    if (args.length > 2) {
                        System.out.println("\t" + countByte + " countByte");
                    }
                }
                if (firstArgument.equals("-m") || firstArgument.equals("--chars")) {
                    int countChars = 0;
                    for (int i = 1; i < args.length; i++) {
                        System.out.println("\t" + getCharCounts(firstArgument) + " " + args[i]);
                        countChars += getCharCounts(args[i]);
                    }
                    if (args.length > 2) {
                        System.out.println("\t" + countChars + " countChars");
                    }
                }
                if (firstArgument.equals("-l") || firstArgument.equals("--lines")) {
                    int countLines = 0;
                    for (int i = 1; i < args.length; i++) {
                        System.out.println("\t" + getLineCounts(args[i]) + " " + args[i]);
                        countLines += getLineCounts(args[i]);
                    }
                    if (args.length > 2) {
                        System.out.println("\t" + countLines + " countLines");
                    }
                }
                if (firstArgument.equals("-L") || firstArgument.equals("--max-line-length")) {
                    int maxLineLength = 0;
                    for (int i = 1; i < args.length; i++) {
                        System.out.println("\t" + getLengthLongestLine(args[i]) + " " + args[i]);
                        if (getLengthLongestLine(args[i]) > maxLineLength) {
                            maxLineLength = getLengthLongestLine(args[i]);
                        }
                    }
                    if (args.length > 2) {
                        System.out.println("\t" + maxLineLength + " maxLineLength");
                    }
                }
                if (firstArgument.equals("-w") || firstArgument.equals("--words")) {
                    int countWords = 0;
                    for (int i = 1; i < args.length; i++) {
                        System.out.println("\t" + getWordCounts(args[i]) + " " + args[i]);
                        countWords += getWordCounts(args[i]);
                    }
                    if (args.length > 2) {
                        System.out.println("\t" + countWords + " countWords");
                    }
                }
            }
        } catch (
                FileNotFoundException error) {
            System.out.println("Error! File Not Found! Check The File Name And " +
                    "Make Sure That A File With That Name Exists!");
        }

    }

    /**
     * Метод отображает (Help) подсказку по командам
     */
    public void printHelp() {
        System.out.println("\n" +
                "wc <filename>\n" +
                "\tprint the number of newlines, words, and chars in file\n" +
                "-c, --bytes\n" +
                "\tprint the byte counts" +
                "-m, --chars\n" +
                "\tprint the character counts\n" +
                "-l, --lines\n" +
                "\tprint the newline counts\n" +
                "-w, --words\n" +
                "\tprint the word counts\n" +
                "--help\n" +
                "\tdisplay this help and exit\n" +
                "--version\n" +
                "\toutput version information and exit");
    }

    /**
     * Метод отображает версию приложения
     */
    public void printVersion() {
        System.out.println("This program duplicates the functionality " +
                "of the Linux WC program. Version 1.0.0 (2022)");
    }

    /**
     * Метод возвращает количество байт в файле
     */
    public int getByteCounts(String string) throws IOException {

        int byteCounts = 0;
        FileInputStream reader = new FileInputStream(string);

        int symbol = reader.read();
        while (symbol != -1) {
            symbol = reader.read();
            byteCounts++;
        }
        reader.close();
        return byteCounts;
    }

    /**
     * Метод возвращает количество символов в файле
     */
    public int getCharCounts(String string) throws IOException {

        int charCounts = 0;
        FileReader reader = new FileReader(string);

        int symbol = reader.read();
        while (symbol != -1) {
            symbol = reader.read();
            charCounts++;
        }
        reader.close();
        return charCounts;
    }

    /**
     * Метод возвращает количество строк в файле
     */
    public int getLineCounts(String string) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(string));
        String line = reader.readLine();

        int lineCounts = 0;
        while (line != null) {
            line = reader.readLine();
            lineCounts++;
        }
        reader.close();
        return lineCounts;
    }

    /**
     * Метод возвращает длину самой длинной линии в файле
     */
    public int getLengthLongestLine(String string) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(string));
        String lineMax = reader.readLine();

        int maxLength = 0;
        while (lineMax != null) {

            if (lineMax.length() > maxLength) {
                maxLength = lineMax.length();
            }
            lineMax = reader.readLine();
        }
        reader.close();
        return maxLength;
    }

    /**
     * Метод возвращает количество слов в файле
     */
    public int getWordCounts(String string) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(string));
        String line = reader.readLine();

        int wordsCount = 0;
        while (line != null) {
            String[] words = line.split(" ");
            wordsCount += words.length;
            line = reader.readLine();
        }
        reader.close();
        return wordsCount;
    }
}