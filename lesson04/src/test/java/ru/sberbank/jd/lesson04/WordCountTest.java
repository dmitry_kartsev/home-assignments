package ru.sberbank.jd.lesson04;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class WordCountTest {
    WordCount wc = new WordCount();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("testing.txt").getFile());
    String absolutePath = file.getAbsolutePath();

    /**
     * Проверяем выполнение теста на вычисление количества байт в файле
     */
    @Test
    public void getByteCounts() throws IOException {
        Assert.assertEquals(102, wc.getByteCounts(absolutePath));
    }

    /**
     * Проверяем выполнение теста на вычисление количества символов в файле
     */
    @Test
    public void getCharCounts() throws IOException {
        Assert.assertEquals(93, wc.getCharCounts(absolutePath));
    }

    /**
     * Проверяем выполнение теста на вычисление количества строк в файле
     */
    @Test
    public void getLineCounts() throws IOException {
        Assert.assertEquals(4, wc.getLineCounts(absolutePath));
    }

    /**
     * Проверяем выполнение теста на вычисление количества
     * символов в самой длинной строке в файле
     */
    @Test
    public void getLengthLongestLine() throws IOException {
        Assert.assertEquals(46, wc.getLengthLongestLine(absolutePath));
    }

    /**
     * Проверяем выполнение теста на вычисление количества слов в файле
     */
    @Test
    public void getWordCounts() throws IOException {
        Assert.assertEquals(16, wc.getWordCounts(absolutePath));
    }
}